<?php

function taobao_admin_settings() {
	global $base_url, $base_path;
	$form['taobao_api_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Taobao Api secret'),
	  '#required' => true,
		'#default_value' => variable_get('taobao_api_secret', 0),
		'#description' => l('http://my.open.taobao.com', 'http://my.open.taobao.com'),
	);
	$form['taobao_api_app_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Taobao Api key'),
	  '#required' => true,
		'#default_value' => variable_get('taobao_api_app_key', 0),
		'#description' => l('http://my.open.taobao.com', 'http://my.open.taobao.com'),
	);
	$form['taobao_alimama_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Alimama id'),
		'#default_value' => variable_get('taobao_alimama_id', 0),
		'#description' => t('Your !Alimama account, e.g: mm_10026546_0_0, so that you will get benefits. If you do not, please leave blank.', array('!Alimama' => l('Alimama', 'http://www.alimama.com')))
	);
	$form['taobao_api_nick'] = array(
		'#type' => 'textfield',
		'#title' => t('Taobao Nick'),
		'#default_value' => variable_get('taobao_api_nick', 0),
		'#description' => t('Your !Taobao nick, e.g: cneast, so that you will get benefits. If you do not, please leave blank.', array('!Taobao' => l('Taobao', 'http://www.taobao.com')))
	);
	$form['taobao_page_path'] = array(
		'#type' => 'textfield',
		'#title' => t('Page Path'),
		'#required' => true,
		'#default_value' => variable_get('taobao_page_path', 'taobao'),
    '#field_prefix' => $base_url . $base_path,
    '#field_suffix' => '/%',
		'#description' => t('Page path prefix, e.g: %path', array('%path' => $base_url . $base_path . 'taobao/demo'))
	);
	return system_settings_form($form);
}

function taobao_admin_export($tid) {
	if ($taobao = db_fetch_object(db_query('SELECT title, path, des, args FROM {taobao} WHERE tid = %d', $tid))) {
		$output = '<textarea rows="15" cols="100">'.json_encode($taobao).'</textarea>';
		$output .= '<p align="right">'.l(t('Demo link'), variable_get('taobao_page_url', 'taobao') . '/' . $taobao->path) . '</p>';
		drupal_set_title($taobao->title);
	}
	return $output;
}

function taobao_admin_import() {
	$form['#action'] = url('admin/settings/taobao/add', array('query' => 'op=import'));
	$form['import'] = array(
		'#type' => 'textarea',
		'#required' => true,
		'#title' => t('Value'),
	);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );
	return $form;
}

function taobao_admin_delete($tid) {
	if ($_GET['op'] == 'delete') {
		db_query('DELETE FROM {taobao} WHERE tid = %d', $tid);
		if (db_affected_rows()) {
			drupal_set_message(t('Operation Successful.'));
		}
		drupal_goto('admin/settings/taobao/lists');
	}
	$output = l(t('Confirm delete'), 'admin/settings/taobao/delete/' . $tid, array('query' => 'op=delete'));
	return $output;
}

function taobao_admin_form() {
	if ($tid = arg(4)) {
		if ($taobao = db_fetch_object(db_query('SELECT * FROM {taobao} WHERE tid = %d', $tid))) {
			$taobao->args = unserialize($taobao->args);
		}
	}
	$form['tid'] = array('#type' => 'hidden', '#value' => $taobao->tid);
	
	if ($_GET['op'] == 'import' && $_POST['import']) {
		$taobao = json_decode(trim($_POST['import']));
		$taobao->args = unserialize($taobao->args);
	}
  $form['method'] = array(
    '#title' => t('Api type'),
    '#default_value' => $taobao->method,
    '#type' => 'select',
    '#required' => true,
    '#options' => array(
    	'taobao.taobaoke.items.get' => '淘宝客商品推广列表',
    )
  );
  $form['title'] = array(
    '#title' => t('Page title'),
    '#default_value' => $taobao->title,
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('Length is less than %len characters.', array('%len' => 64))
  );
  $form['des'] = array(
    '#title' => t('Page description'),
    '#default_value' => $taobao->des,
    '#type' => 'textfield',
    '#description' => t('Length is less than %len characters.', array('%len' => 255))
  );
  $form['path'] = array(
    '#title' => t('Page path'),
    '#default_value' => $taobao->path,
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('Length is less than %len characters.', array('%len' => 60))
  );
	$form['args'] = array(
		'#title' => t('Settings'),
		'#type' => 'fieldset',
		'#collapsible' => true,
    '#tree' => true,
	);
  $form['args']['keyword'] = array(
    '#title' => t('Keywords'),
    '#default_value' => $taobao->args['keyword'],
    '#type' => 'textfield',
    '#description' => '商品关键词，如：耐克'
  );
  $form['args']['cid'] = array(
    '#title' => '商品分类 ID',
    '#default_value' => $taobao->args['cid'],
    '#type' => 'textfield',
    '#description' => '如何获取此值？请参看此页：'. l('淘宝分类工具', 'http://open.taobao.com/api_tool/props/') . '。关键词、分类 ID必填一项。'
  );
  $form['args']['start_price'] = array(
    '#title' => '商品价格大于',
    '#default_value' => $taobao->args['start_price'],
    '#type' => 'textfield',
    '#field_suffix' => ' 元',
  );
  $form['args']['end_price'] = array(
    '#title' => '商品价格小于',
    '#default_value' => $taobao->args['end_price'],
    '#field_suffix' => ' 元',
    '#type' => 'textfield',
    '#description' => '商品最小、最大价格必须同时赋值，或同时留空。'
  );
  $form['args']['start_commission'] = array(
    '#title' => '佣金大于',
    '#default_value' => $taobao->args['start_commission'],
    '#type' => 'textfield',
    '#field_suffix' => ' 元',
    '#description' => '佣金最小为此值'
  );
  $form['args']['end_commission'] = array(
    '#title' => t('佣金小于'),
    '#default_value' => $taobao->args['end_commission'],
    '#type' => 'textfield',
    '#field_suffix' => ' 元',
    '#description' => '佣金最大为此值'
  );
  $form['args']['start_commissionRate'] = array(
    '#title' => '佣金比例大于',
    '#default_value' => $taobao->args['start_commissionRate'],
    '#type' => 'textfield',
    '#field_suffix' => ' %',
    '#description' => '佣金最小比例为此值'
  );
  $form['args']['end_commissionRate'] = array(
    '#title' => t('佣金比例小于'),
    '#default_value' => $taobao->args['end_commissionRate'],
    '#type' => 'textfield',
    '#field_suffix' => ' %',
    '#description' => '佣金最大比例为此值'
  );
  $form['args']['start_commissionNum'] = array(
    '#title' => '最低累计推广量',
    '#default_value' => $taobao->args['start_commissionNum'],
    '#type' => 'textfield',
    '#field_suffix' => ' 件',
  );
  $form['args']['end_commissionNum'] = array(
    '#title' => t('最高累计推广量'),
    '#default_value' => $taobao->args['end_commissionNume'],
    '#type' => 'textfield',
    '#field_suffix' => ' 件',
  );
  $form['args']['area'] = array(
    '#title' => '商品所在城市',
    '#default_value' => $taobao->args['area'],
    '#type' => 'textfield',
    '#description' => '例如：杭州市'
  );
  
  $credits = array('' => t('default')) + drupal_map_assoc(array('1heart', '2heart', '3heart', '4heart', '5heart', '1diamond', '2diamond', '3diamond', '4diamond', '5diamond', '1crown', '2crown', '3crown', '4crown', '5crown', '1goldencrown', '2goldencrown', '3goldencrown', '4goldencrown', '5goldencrown'));
  
  $form['args']['start_credit'] = array(
    '#title' => '卖家信用大于',
    '#default_value' => $taobao->args['start_credit'],
    '#type' => 'select',
    '#options' => $credits
  );
  $form['args']['end_credit'] = array(
    '#title' => '卖家信用小于',
    '#default_value' => $taobao->args['end_credit'],
    '#type' => 'select',
    '#description' => '最小、最大信用必须同时赋值，或同时留空。',
    '#options' => $credits
  );
  
  $form['args']['sort'] = array(
    '#title' => t('Sort '),
    '#default_value' => $taobao->args['sort'],
    '#type' => 'select',
    '#options' => array(
      '' => t('default'),
      'price_desc' => '价格从高到低',
      'price_asc' => '价格从低到高',
      'credit_desc' => '信用等级从高到低',
      'commission_desc' => '佣金从高到底',
      'commission_asc' => '佣金从低到高',
      'commissionRate_desc' => '佣金比率从高到底',
      'commissionRate_asc' => '佣金比率从低到高',
      'commissionNum_desc' => '成交量成高到低',
      'commissionNum_asc' => '成交量从低到高',
      'commissionVolume_desc' => '总支出佣金从高到底',
      'commissionVolume_asc' => '总支出佣金从低到高',
      'delistTime_desc' => '商品下架时间从高到底',
      'delistTime_asc' => '商品下架时间从低到高'
    ),
  );
  $form['args']['auto_send'] = array(
    '#title' => '自动发货？',
    '#default_value' => $taobao->args['auto_send'],
    '#type' => 'checkbox',
    '#optionts' => array(1)
  );
  $form['args']['is_guarantee '] = array(
    '#title' => '消保卖家？',
    '#default_value' => $taobao->args['is_guarantee '],
    '#type' => 'checkbox',
    '#optionts' => array(1)
  );
  $form['args']['page_size'] = array(
    '#title' => '每页显示记录',
    '#default_value' => $taobao->args['page_size'],
    '#type' => 'textfield',
    '#description' => '最多 50 条',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#submit' => array('taobao_admin_form_submit'),
    '#value' => t('Save'),
  );
  $form['#validate'][] = 'taobao_admin_form_validate';
	return $form;
}

function taobao_admin_form_validate($form, &$form_state) {
	$v = $form_state['values'];
	foreach ($v['args'] as $key => $val) {
		if ($val && in_array($key, array('start_price', 'end_price', 'start_commission', 'end_commission', 'start_commissionRate', 'end_commissionRate', 'start_commissionNum', 'end_commissionNum', 'page_size')) && !is_numeric($val)) {
			form_set_error($key, t('Can only enter numbers'));
		}
	}
	if (!$v['args']['keyword'] && !$v['args']['cid']) {
		form_set_error('keyword', '关键词或商品分类 id 必填一项');
	}
	if (drupal_strlen($v['title']) > 64) {
		form_set_error('title', t('Please re-enter'));
	}
	if (drupal_strlen($v['path']) > 60) {
		form_set_error('path', t('Please re-enter'));
	}
	if (drupal_strlen($v['des']) > 255) {
		form_set_error('des', t('Please re-enter'));
	}
	if ($v['tid']) {
		if (db_result(db_query("SELECT path FROM {taobao} WHERE tid != %d AND path = '%s'", $v['tid'], trim($v['path'])))) {
			form_set_error('path', t('Path already exists'));
		}
	} else {
		if (db_result(db_query("SELECT path FROM {taobao} WHERE path = '%s'", trim($v['path'])))) {
			form_set_error('path', t('Path already exists'));
		}
	}
}

function taobao_admin_form_submit($form, &$form_state) {
	$v = $form_state['values'];
	if ($v['tid']) {
		db_query("UPDATE {taobao} SET title = '%s', des = '%s', method = '%s', path = '%s', args = '%s' WHERE tid = %d", $v['title'], $v['des'], $v['method'], trim($v['path']), serialize($v['args']), $v['tid']);
		if (db_affected_rows()) {
			drupal_set_message(t('Operation Successful.'));
			drupal_goto(variable_get('taobao_page_url', 'taobao') . '/' . $v['path']);
		} else {
			drupal_set_message(t('Operation failed.'));
		}
	} else {
		db_query("INSERT INTO {taobao} (tid, title, des, method, path, args) VALUES (null, '%s', '%s', '%s', '%s', '%s')", $v['title'], $v['des'], $v['method'], trim($v['path']), serialize($v['args']));
		if ($tid = db_last_insert_id('taobao', 'tid')) {
			drupal_set_message(t('Operation Successful.'));
			drupal_goto('admin/settings/taobao/lists/');
		} else {
			drupal_set_message(t('Operation failed.'));
		}
	}
}

function taobao_admin_lists() {
	$path = variable_get('taobao_page_url', 'taobao') . '/';
	$result = pager_query('SELECT * FROM {taobao} ORDER BY tid DESC', 10);
	while($o = db_fetch_object($result)) {
		$args = unserialize($o->args);
		foreach ($args as $key => $val) {
			if ($val != "") $item[] = $key . ': ' . $val;
		}
		$table[] = array(
			l($o->title, $path . $o->path), $o->des, theme('item_list', $item),
			theme('item_list', array(
					l(t('view'), $path . $o->path),
					l(t('Edit'), 'admin/settings/taobao/edit/'. $o->tid),
					l(t('Delete'), 'admin/settings/taobao/delete/'. $o->tid),
					l(t('Export'), 'admin/settings/taobao/export/'. $o->tid)
				)
			),
		);
	}
	$output = '';
	$output .= theme('table', array(t('Page title'), t('Page description'), t('Parameters'), t('Operation')), $table);
	return $output;
}