<?php

/**
 * 淘客商品展示页面
 * @param (string) $path
 *  自定义路径
 */
function taobao_page_args($path) {
	if ($o = db_fetch_object(db_query("SELECT * FROM {taobao} WHERE path = '%s'", trim($path)))) {
		$o->args = unserialize($o->args);
		if ($pid = variable_get('taobao_alimama_id', 0)) {
			$o->args['pid'] = $pid; // 淘客 id
		}
		
		$data = taobao_get_taoke_items($o->args);
		
		if ($data->taobaoke_items_get_response->taobaoke_items->taobaoke_item) {
			taobao_pager_lists($data->taobaoke_items_get_response->total_results, $o->args['page_size'] ? $o->args['page_size'] : 20);
			
			$credit = taobao_get_users($data->taobaoke_items_get_response->taobaoke_items->taobaoke_item);
			
			foreach ($data->taobaoke_items_get_response->taobaoke_items->taobaoke_item as $item) {
				$item->wangwang = taobao_get_wangwang($item->nick);
				$item->credit = $credit[$item->nick];
				$lists[] = theme('taobao_teaser', $item);
			}
			
			$output = theme('taobao_lists', $o, $lists);
			$output .= theme_pager();
			
		} else {
		  
			$output = t('No Data');
			
		}
		
		drupal_set_title($o->title);
	} else {
		drupal_not_found();
	}
	
	return $output;
}