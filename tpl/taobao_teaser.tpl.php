<?php
	// print_r($v); // 查看全部数据，包括佣金额度、提成比例、销售数量等
	/*
		$v->pict_url: 商品主图片，加上 '_sum.jpg' 为小图, 加 '_b.jpg' 为中图
	*/
?>
<table>
	<tr>
  	<td colspan="2">
  		<h2><a href="<?php echo $v->click_url; ?>" "target"="_blank"><?php echo $v->title; ?></a></h2>
  	</td>
	</tr>
	<tr>
  	<td width="200">
  		<a href="<?php echo $v->pic_url; ?>"><img src="<?php echo $v->pic_url; ?>_b.jpg" /></a>
  	</td>
  	<td>
  		<ul>
  			<li>价格： <?php echo $v->price; ?> 元</li>
  			<li>卖家：<a href="<?php echo $v->shop_click_url?>"><?php echo $v->nick; ?></a></li>
  			<li>信用：<?php echo $v->credit['seller']; ?></li>
  			<li><?php echo $v->wangwang; ?></li>
  		</ul>
  	</td>
	</tr>
</table>